import { UserConfig } from './Config'
import { CustomScene } from './CustomScene'
import { CustomSprite } from './CustomSprite'
import { Event } from './Event'
import { load } from './SaveSystem'

export class Player extends CustomSprite {
  public static loadSave(): Record<string, any> {
    return load(UserConfig.saveName)
  }

  public static load({
    pos,
  }: {
    scene?: CustomScene
    event?: Event
    pos?: Record<string, number>
  }): Player {
    if (Player.instance) {
      if (pos) {
        Player.instance.setPosition(pos.x, pos.y)
      }
      return Player.instance
    }
    return Player.instance
  }

  private static instance: Player
  private _health: Record<string, number>
  private _bangInfos: Record<string, number>
  
  public get bangInfos(): Record<string, number> {
    return this._bangInfos
  }
  public set bangInfos(value: Record<string, number>) {
    this._bangInfos = value
  }

  public get health(): Record<string, number> {
    return this._health
  }

  public set health(v: Record<string, number>) {
    this._health = v
  }

  private constructor(
    public readonly scene: CustomScene,
    x: number,
    y: number,
    texture: string = 'player',
    frame: number = 1
  ) {
    super(scene, x, y, texture, frame)

    this.setDepth(2)
  }

}
