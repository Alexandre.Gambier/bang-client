import i18n from 'i18next'

import fr from '../locales/fr-FR/translation.json'

i18n.init({
  lng: 'fr-FR',
  fallbackLng: 'fr-FR',
  locales: ['fr-FR'],
  resources: {
    'fr-FR': {
      translation: fr,
    },
  },
})

export { i18n }
