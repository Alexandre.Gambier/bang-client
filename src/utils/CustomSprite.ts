import { Scene } from 'phaser'

export class CustomSprite extends Phaser.Physics.Arcade.Sprite {
  constructor(
    scene: Scene,
    x: number,
    y: number,
    texture: string,
    frame: number = 1
  ) {
    super(scene, x, y, texture, frame)
    scene.add.existing(this)
    scene.physics.add.existing(this)
  }
}
