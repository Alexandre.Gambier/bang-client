import { CustomScene } from './CustomScene'


export enum EVENT_TYPE {
  MAP_CHANGE = 'mapChange',
  TALK = 'talk',
  FIGHT = 'fight',
}

export class Event extends Phaser.GameObjects.GameObject {
  public x: number
  public y: number
  public type: string
  public body: Phaser.Physics.Arcade.Body
  public properties: Record<string, any>

  constructor(
    public readonly scene: CustomScene,
    event: Phaser.GameObjects.GameObject & Record<string, any>
  ) {
    super(scene, event.type || 'customEvent')
  }
}
