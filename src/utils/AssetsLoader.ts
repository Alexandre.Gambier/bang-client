import saloonBGM from '@/assets/audio/bgm/saloon.mp3'
import title from '@/assets/backgrounds/title.png'
import titleBack from '@/assets/backgrounds/title_back.jpg'
import logo from '@/assets/logo.png'
import buttonBlue from '@/assets/system/buttons_blue.png'
import buttonDisabled from '@/assets/system/buttons_disabled.png'
import buttonGreen from '@/assets/system/buttons_green.png'
import buttonRed from '@/assets/system/buttons_red.png'
import buttonContinue from '@/assets/system/Button_Continue.png'
import buttonContinueHL from '@/assets/system/Button_ContinueHL.png'
import panelDialog from '@/assets/system/Panel_Dialogue.png'
import panelName from '@/assets/system/Panel_Name.png'


class Asset {
  constructor(
    public readonly key: string,
    public readonly value: string,
    public readonly type: string,
    public readonly config?: Phaser.Loader.FileTypes.ImageFrameConfig
  ) {}
}

export const Assets = [
  new Asset('logo', logo, 'image'),
  new Asset('window', panelDialog, 'image'),
  new Asset('name_panel', panelName, 'image'),
  new Asset('continue_button', buttonContinue, 'image'),
  new Asset('continue_button_hl', buttonContinueHL, 'image'),
  new Asset('title_text', title, 'image'),
  new Asset('title_back', titleBack, 'image'),
  new Asset('button_disabled', buttonDisabled, 'image'),

  new Asset('button_red', buttonRed, 'spritesheet', {
    frameWidth: 73,
    frameHeight: 27,
  }),
  new Asset('button_blue', buttonBlue, 'spritesheet', {
    frameWidth: 73,
    frameHeight: 27,
  }),
  new Asset('button_green', buttonGreen, 'spritesheet', {
    frameWidth: 73,
    frameHeight: 26.6,
  }),

  new Asset('title_bgm', saloonBGM, 'audio'),

]
