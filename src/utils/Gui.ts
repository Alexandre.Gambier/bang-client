import { GameObjects, Scene } from 'phaser'
import { Slider } from 'phaser3-rex-plugins/templates/ui/ui-components.js'

export enum COLORS {
  COLOR_PRIMARY = 0x4e342e,
  COLOR_LIGHT = 0x7b5e57,
  COLOR_DARK = 0x260e04,
  COLOR_WHITE = 0xffffff,
  COLOR_GRAY = 0x808080,
  COLOR_GREEN = 0x00cc00,
  COLOR_GREEN_DARK = 0x009900,
  COLOR_BLACK = 0x000000,
  COLOR_BLUE = 0x00a3cc,
  COLOR_BLUE_DARK = 0x007a99,
}

export class Gui {
  private elements: GameObjects.Container

  /**
   *
   * @param scene {Scene} The parent of the gui
   * @param x number The x position of the gui in pixels
   * @param y number The y position of the gui in pixels
   * @param width number The width of the main frame in pixels
   * @param height number The height of the main frame in pixels
   * @param rotate number The rotation of the frame in degree
   */
  constructor(
    private readonly scene: Scene,
    {
      x = 0,
      y = 0,
      width = 0,
      height = 0,
      rotate = 0,
      origin = { x: 0.5, y: 0.5 },
      backgroundImage,
      backgroundColor = 0x000000,
    }: {
      x?: number
      y?: number
      width?: number
      height?: number
      rotate?: number
      origin?: { x?: number; y?: number }
      backgroundImage?: string
      backgroundColor?: number
    }
  ) {
    const radRotate = (rotate * Math.PI) / 180
    this.elements = scene.add.container(x, y)
    if (backgroundImage) {
      const background = scene.add
        .image(0, 0, 'window')
        .setOrigin(origin.x, origin.y)
        .setRotation(radRotate)
        .setDisplaySize(width, height)

      this.elements.add(background)
    } else {
      const background = scene.add.graphics()
      background.fillStyle(backgroundColor, 0.4)
      background.setRotation(radRotate)

      background.fillRect(0 - width / 2, 0 - height / 2, width, height)
      this.elements.add(background)
    }
    this.elements.setDepth(100)
  }

  public getElements(): GameObjects.GameObject[] {
    return this.elements.getAll()
  }

  public getLastElement<T extends GameObjects.GameObject>(): T {
    return this.elements.getAll()[this.elements.length - 1] as T
  }

  public createDialog({
    text,
    x = 0,
    y = 0,
    style = {
      fontColor: 'black',
      fontSize: '1.3vw',
    },
  }: {
    text: string
    x?:number
    y?: number
    style?: Record<string, any>
  }) {
    const { scene } = this
    const drawedText = scene.add.text(x, y, text, style)
    this.elements.add(drawedText)

    return this
  }

  public createSlider({
    x,
    y,
    width,
    height,
    value = 0,
    origin = { x: 0.5, y: 0.5 },
    callback,
  }: {
    x: number
    y: number
    width: number
    height: number
    value?: number
    origin?: { x: number; y: number }
    callback: (sliderValue: number) => any
  }) {
    const {scene} = this
    const img = scene.add.rectangle(x, y, width, height, 0x666666)
    const track = scene.add.rectangle(x, y, 10, 40, COLORS.COLOR_WHITE)

    const indicator = scene.add.rectangle(x, y, 10, 40, COLORS.COLOR_PRIMARY)

    const slider = new Slider(scene, {
      x,
      y,
      width,
      enable: true,
      value,
      input: 'click',
      track,
      indicator,
      background: img,
    })
      .setOrigin(origin.x, origin.y)
      .layout()

    track
      .setInteractive()
      .on('pointerdown', (pointer: any, localX: number, localY: number) => {
        track.on('pointermove', (pointer: any, localX: number) => {
          const sliderValue = localX / 10
          slider.setValue(sliderValue)
          callback(sliderValue)
        })
      })
      .on('pointerup', () => {
        track.off('pointermove')
      })

    this.elements.add(slider)
  }

  public addLabel({
    x,
    y,
    text,
    style = {
      fontColor: 'black',
      fontSize: '1.3vw',
    },
    origin = { x: 0.5, y: 0.5 },
  }: {
    x: number
    y: number
    text: string
    style?: Record<string, any>
    origin?: { x: number; y: number }
  }) {
    const label = this.scene.add
      .text(x, y, text, style)
      .setOrigin(origin.x, origin.y)
      .setDepth(1)
    this.elements.add(label)
    return this
  }

  public addButton({
    x,
    y,
    text,
    width,
    callback,
    active = false,
    style = {
      fontColor: 'black',
      fontSize: '1.3vw',
    },
    customButton,
    buttonColor = COLORS.COLOR_GREEN,
    overColor = COLORS.COLOR_GREEN_DARK,
    customPointerEvent = 'pointerup',
  }: {
    x: number
    y: number
    width?: number
    text: string
    callback: (button: Record<string, any>) => any
    active?: boolean
    style?: Record<string, any>
    customButton?: string
    overColor?: number | COLORS
    buttonColor?: number | COLORS
    customPointerEvent?: string
  }): Gui {
    let buttonBack: GameObjects.Image | GameObjects.Rectangle = null
    const buttonText = this.scene.add.text(x, y, text, style).setDepth(1)
    const widthDisplay = width || 1.5 * buttonText.displayWidth
    const height = buttonText.displayHeight * 1.5
    if (active) {
      if (customButton) {
        buttonBack = this.scene.add
          .image(x + widthDisplay / 3, y + height / 2.5, customButton)
          .setDisplaySize(widthDisplay, height)
          .setDepth(0)
      } else {
        buttonBack = this.scene.add.rectangle(
          x + widthDisplay / 3,
          y + height / 2.5,
          widthDisplay,
          height,
          buttonColor
        )
      }
      buttonBack
        .setInteractive()
        .on(customPointerEvent, () => {
          if (buttonBack instanceof GameObjects.Image) {
            buttonBack.setFrame(1)
          } else {
            buttonBack.setFillStyle(COLORS.COLOR_BLACK)
          }
          callback({ buttonBack, buttonText })
        })
        .on('pointerdown', () => {
          if (buttonBack instanceof GameObjects.Image) {
            buttonBack.setFrame(2)
          } else {
            buttonBack.setFillStyle(COLORS.COLOR_BLACK)
          }
        })
        .on('pointerover', () => {
          if (buttonBack instanceof GameObjects.Image) {
            buttonBack.setFrame(1)
          } else {
            buttonBack.setFillStyle(overColor)
          }
        })
        .on('pointerout', () => {
          if (buttonBack instanceof GameObjects.Image) {
            buttonBack.setFrame(0)
          } else {
            buttonBack.setFillStyle(buttonColor)
          }
        })
    } else {
      if (customButton) {
        buttonBack = this.scene.add
          .sprite(x + widthDisplay / 3, y + height / 3.5, 'button_disabled')
          .setDisplaySize(widthDisplay, height)
          .setDepth(0)
      } else {
        buttonBack = this.scene.add.rectangle(
          x + widthDisplay / 3,
          y + height / 2.5,
          widthDisplay,
          height,
          COLORS.COLOR_GRAY
        )
      }
    }

    if (buttonBack) {
      this.elements.add(buttonBack)
    }

    this.elements.add(buttonText)

    return this
  }

  public empty() {
    this.elements.each((guiItem: GameObjects.GameObject, index: number) => {
      if (index > 0) {
        guiItem.destroy()
      }
    })
  }

  public destroy() {
    this.elements.each((guiItem: GameObjects.GameObject) => {
      guiItem.destroy()
    })
    this.elements.destroy()
  }
}
