import { GameObjects, Scale, Scene } from 'phaser'
import { BootScene } from '../scenes/BootScene'
import { PlayScene } from '../scenes/PlayScene'
import { TitleScene } from '../scenes/TitleScene'
import { COLORS, Gui } from './Gui'
import { i18n } from './I18n'
import { loadConfig, saveConfig } from './SaveSystem'
import { vwToPixel } from './Tools'


export const width: number = window.innerWidth
export const height: number = window.innerHeight

export const defaultUserConfig = {
  version: '0.0.1',
  audio: {
    master: 0.15,
    bgm: 1,
    bgs: 1,
  },
  saveName: 'save',
}

export const UserConfig = loadConfig()

export const Config: GameConfig = {
  width,
  height,
  type: Phaser.AUTO,
  parent: 'app',
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
    },
  },
  fps: {
    min: 30,
    target: 60,
  },
  render: { pixelArt: true, antialias: false },
  scene: [BootScene, TitleScene, PlayScene],
  scale: Scale.MAX_ZOOM,
}

export const showOptions = (scene: Scene, returnCallback: CallableFunction) => {
  const menuHeight = height
  const menuWidth = width / 6
  const volumeText = i18n.t('config.audio.base')
  const returnText = i18n.t('commons.return')
  const fontSize = 1.3
  const style = {
    fontSize: `${fontSize}vw`,
    align: 'center',
  }
  const menu = new Gui(scene, {
    x: menuWidth / 2,
    y: menuHeight / 2,
    width: menuWidth,
    height: menuHeight,
  })

  let config = drawConfigPanel(scene, menuHeight, menuWidth)

  let index = 0

  menu
    .addButton({
      x: -(volumeText.length / 2 + 1) * (vwToPixel(fontSize) / 2),
      y: -menuHeight / 2.5 + (50 + vwToPixel(fontSize)) * index++,
      text: volumeText,
      style,
      callback: () => {
        config.destroy()
        config = drawConfigPanel(scene, menuHeight, menuWidth)
        showAudio(
          scene,
          config,
          menuHeight,
          menuWidth,
          vwToPixel(fontSize),
          style
        )
      },
      active: true,
    })
    .addButton({
      x: -(returnText.length / 2 + 1) * (vwToPixel(fontSize) / 2),
      y: -menuHeight / 2.5 + (50 + vwToPixel(fontSize)) * index++,
      text: returnText,
      style,
      callback: () => {
        config.destroy()
        menu.destroy()
        returnCallback()
      },
      buttonColor: COLORS.COLOR_BLUE,
      overColor: COLORS.COLOR_BLUE_DARK,
      active: true,
    })
}

const drawConfigPanel = (
  scene: Scene,
  menuHeight: number,
  menuWidth: number
): Gui => {
  return new Gui(scene, {
    x: width - width / 2.5,
    y: menuHeight / 2,
    width: width - menuWidth,
    height: menuHeight,
  })
}

const showAudio = (
  scene: Scene,
  config: Gui,
  menuHeight: number,
  menuWidth: number,
  fontSize: number,
  style: Record<string, any>
) => {
  Object.keys(UserConfig.audio).forEach((key, index) => {
    const text = i18n.t(`config.audio.${key}`, { value: UserConfig.audio[key] })
    const labelX = -width / 2.5 + 2 * text.length
    const y = -menuHeight / 2.5 + (50 + fontSize) * index
    config.addLabel({
      x: labelX,
      y,
      text,
      style,
      origin: { x: 0, y: 0 },
    })

    const label = config.getLastElement<GameObjects.Text>()

    config.createSlider({
      x: width / 2,
      y: menuHeight / 10 + (50 + fontSize) * index,
      origin: { x: 0, y: 0 },
      width: width / 4,
      height: 20,
      value: UserConfig.audio[key],
      callback: soundValue => {
        UserConfig.audio[key] = soundValue.toFixed(2)
        saveConfig(UserConfig)
        label.setText(
          i18n.t(`config.audio.${key}`, { value: UserConfig.audio[key] })
        )
        const {
          audio: { bgm, master },
        } = UserConfig
        scene.sound.volume = bgm * master
      },
    })
  })
}
