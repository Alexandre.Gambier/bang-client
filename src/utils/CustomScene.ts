import { Scene } from 'phaser'
import { UserConfig } from './Config'
import { Gui } from './Gui'
import { Player } from './Player'

export class CustomScene extends Scene {
  protected _isPaused: boolean = false
  private _isDialogOpen: boolean = false
  protected _player: Player
  protected _players: Player[]
  protected _bgm: Record<string, any>
  
  protected _CAMERA_ZOOM = 1
  protected _menu: Gui
  protected _dialog: Gui
  
  
  public get dialog(): Gui {
    return this._dialog
  }
  public set dialog(value: Gui) {
    this._dialog = value
  }

  public get isDialogOpen(): boolean {
    return this._isDialogOpen
  }
  public set isDialogOpen(value: boolean) {
    this._isDialogOpen = value
  }

  public get CAMERA_ZOOM(): number {
    return this._CAMERA_ZOOM
  }

  public set CAMERA_ZOOM(CAMERA_ZOOM: number) {
    this._CAMERA_ZOOM = CAMERA_ZOOM
  }

  public get menu(): Gui {
    return this._menu
  }

  public set menu(menu: Gui) {
    this._menu = menu
  }

  public get isPaused(): boolean {
    return this._isPaused
  }

  public set isPaused(v: boolean) {
    this._isPaused = v
  }

  public get bgm(): Record<string, any> {
    return this._bgm
  }

  public set bgm(v: Record<string, any>) {
    this._bgm = v
  }

  public get player(): Player {
    return this._player
  }

  public set player(v: Player) {
    this._player = v
  }

  public setSceneMusic(musicName: string, config: SoundConfig) {
    this.bgm = {mainMusic: musicName}
    this.sound.play(musicName, config)
    const {
      audio: { master, bgm },
    } = UserConfig
    this.sound.volume = master * bgm
  }

  public stopSceneMusic() {
    this.sound.stopAll()
  }
}
