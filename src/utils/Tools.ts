export const vwToPixel = (value: number): number => {
  return (window.innerWidth * value) / 100
}
