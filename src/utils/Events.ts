import { CustomScene } from './CustomScene'
import { Event } from './Event'
export class Events {
  private _events: Event[]

  constructor(
    private readonly scene: CustomScene,
    layer: Phaser.Tilemaps.ObjectLayer
  ) {
    this._events = layer.objects.map(event => new Event(scene, event))
  }

  public get events(): Record<string, any> {
    return this._events
  }

  public getEventByName(name: string): Event {
    return this.events.find(
      (event: Record<string, any>) => event.name === 'spawn'
    )
  }

  public getEventsByType(type: string): Event[] {
    return this.events.filter((event: Event) => event.type === type)
  }

  public destroy() {
    this.events.forEach((event: Event) => {
      event.destroy()
    })
  }
}
