import { CustomScene } from '../utils/CustomScene'


export class PlayScene extends CustomScene {
  public static SCENE_NAME: string = 'PlayScene'

  constructor() {
    super({ key: PlayScene.SCENE_NAME })
  }

  public create({ mapName, ...others }: { mapName: string; others: any[] }) {
    
  }

  public update(time: number, delta: any) {
    
  }
}
