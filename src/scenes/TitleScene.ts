import { height, showOptions, UserConfig, width } from '../utils/Config'
import { CustomScene } from '../utils/CustomScene'
import { COLORS, Gui } from '../utils/Gui'
import { i18n } from '../utils/I18n'
import { clearSave } from '../utils/SaveSystem'
import { vwToPixel } from '../utils/Tools'
import { PlayScene } from './PlayScene'



export class TitleScene extends CustomScene {
  public static SCENE_NAME: string = 'TitleScene'

  private selected = 0

  constructor() {
    super({ key: TitleScene.SCENE_NAME })
  }

  public create() {
    const background = this.add
      .image(0, 0, 'title_back')
      .setOrigin(0)
      .setDisplaySize(width, height)
    const title = this.add
      .image(0, -width, 'title_text')
      .setOrigin(0)

    const {
      audio: { master, bgm },
    } = UserConfig

    this.setSceneMusic('title_bgm', {
      loop: true,
    })

    const titleTween = this.tweens.add({
      targets: title,
      y: 0,
      duration: 2000,
      ease: 'Power2',
      yoyo: false,
      onComplete: this.showTitle.bind(this),
    })
  }

  public showTitle() {
    const menuHeight = height / 4
    const menuWidth = width / 6
    const newText = i18n.t('commons.newGame')
    const optionsText = i18n.t('commons.options')
    const fontSize = 1.3
    const style = {
      fontSize: `${fontSize}vw`,
      align: 'center',
    }
    const newtextX = 0 - (newText.length / 4 + 1) * (vwToPixel(fontSize) / 2)
    const optionX = 0 - (optionsText.length / 2 + 1) * (vwToPixel(fontSize) / 2)

    const menu = new Gui(this, {
      x: width - width / 2,
      y: height - menuHeight,
      width: menuWidth,
      height: menuHeight,
      rotate: 90,
    })
    menu
      .addButton({
        x: newtextX,
        y: 0 - menuHeight / 2,
        text: newText,
        style,
        callback: () => {
          clearSave(UserConfig.saveName)
          this.scene.start(PlayScene.SCENE_NAME, { mapName: 'camp' })
          this.stopSceneMusic()
        },
        active: true,
      })
      .addButton({
        x: optionX,
        y: 0 + menuHeight / 4,
        text: optionsText,
        style,
        callback: () => {
          menu.destroy()
          showOptions(this, this.showTitle.bind(this))
        },
        buttonColor: COLORS.COLOR_BLUE,
        overColor: COLORS.COLOR_BLUE_DARK,
        active: true,
      })
  }
}
