init:
	docker-compose run --rm bang-client npm i

start:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose run --rm bang-client npm run build

logs:
	docker logs bang-client -f